## Student app project

### Tech base
* Node.js >8
* React
* Firebase

### Get Start
* `yarn start` start for developer
* `yarn build` start for build

## Folder Structure
```
student-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    App.css
    App.js
    App.test.js
    index.css
    index.js
    logo.svg
```