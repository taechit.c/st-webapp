import axios from 'axios';
import APP_CONFIG from '../configs/config';

const questionSubmit = (form) => {
  console.log(form);
  if(form["status"]==="1"){
    if(form["projectname"] === undefined){
      form["projectname"] = "-";
    }
    if(form["faculty"] === undefined){
      form["faculty"] = "-";
    }
    if(form["major"] === undefined){
      form["major"] = "-";
    }
    if(form["uniText"] === undefined){
      form["uniText"] = "-";
    }
    if(form["facText"] === undefined){
      form["facText"] = "-";
    }
    if(form["majText"] === undefined){
      form["majText"] = "-";
    }
  }
  
  console.log(form);
  return (dispatch) =>{
      const options = {
          url : `${APP_CONFIG.API_ENDPOINT}`,
          method:'post',
          headers:{
              'Content-Type' : 'application/json'
          },
          data : {...form}
      }
      console.log("Form data");
      console.log(form);
      return axios(options)
      .then(response => {
        //console.log(response);
        dispatch ({
          type: 'SUBMIT_SUCCESS',
          payload: { ...response }
        });
      })
      .catch(err => {
        console.log(err);
        dispatch ({
          type: 'SUBMIT_FAIL',
          payload: { ...err }
        });
      });
  }
}


export {
    questionSubmit
  };