import React from 'react';
import _ from 'lodash';
import { Field, reduxForm, formValueSelector } from 'redux-form';

import fire from '../../configs/fireconfig';
import styles from "./question.scss";

const MAX_NUM = 60;
const MAX_ROOM = 12;

// config validate rule
const validate = (values) => {
  const errors = {};
  if(!values.fname){
    errors.fname = 'Required'
  }
  if(!values.lname){
    errors.lname = 'Required'
  }
  if(!values.nickname){
    errors.nickname = 'Required'
  }
  return errors;
};

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error }
})=>(
    <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched && ((error && <span>{error}</span>))}
      </div>
    </div>
);

class QuestionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status:"0",
      universitys : [],
      facultys : [],
      majors:[],
      university:"",
      faculty : "",
      major :"",
      projectname:""
    };
    this.statusChange = this.statusChange.bind(this);
    this.uniChange = this.uniChange.bind(this);
    this.facultyChange = this.facultyChange.bind(this);
    this.majorChange = this.majorChange.bind(this);
  }

  statusChange(event){
    this.setState({projectname: "-"});
    let uniList = fire.database().ref().child('universitys');
    var uniNamesList = [];
    uniList.on('value', unis => {
        unis.forEach(uniData =>{
          console.log(uniData.key);
          uniNamesList.push(uniData.key);
        })
      });
    this.setState({status: event.target.value});
    this.setState({universitys : uniNamesList})
  }
  uniChange(event){
    var uni = event.target.value;
    var facultyList = fire.database().ref().child('universitys/'+uni);
    var data = [];
    facultyList.on('value', facs => {
      facs.forEach(facData =>{
        data.push(facData.key);
      })
    });
    this.setState({facultys : data});
    this.setState({university : uni})
    this.setState({majors : []});
  }
  facultyChange(event){
    var fac = event.target.value;
    var majorList = fire.database().ref().child('universitys/'+this.state.university +"/"+fac);
    var dataList = [];
    majorList.on('value', majs => {
      this.setState({majors : majs.val()});
      _.forEach(majs.val(),(val,key)=>{
        let inData = [key,val];
        dataList.push(inData);
      });
    });
    this.setState({majors : dataList});
    
  }
  majorChange(event){

  }
 
  render () {
    const { handleSubmit, handleQuestion, value, submitting } = this.props;
    const textfieldStyles = {
      floatingLabelStyle: {
        color: '#FFF'
      },
      hintStyle: {
        color: '#c4c4c4'
      },
      errorStyle: {
        color: '#ffc672'
      }
    };
    let formStatus;
    if(this.state.status ==="1"){
      formStatus = <div id="formStatus"> 
      <div className="form-group">
        <label className="col-sm-3 control-label">มหาวิทยาลัย</label>
        <div className="col-sm-4">
          <Field component="select" name="university" className="form-control" onChange={this.uniChange} >
            <option value ="000" >------ มหาวิทยาลัย -----</option>
              { _.each(this.state.universitys).map(value => <option key={value} value={value}>{value}</option>) }
          </Field>
        </div>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">คณะ</label>
        <div className="col-sm-4">
          <Field component="select" name="faculty" className="form-control" onChange={this.facultyChange}>
              <option value ="000" >------ คณะ -----</option>
              { _.each(this.state.facultys).map(value => <option key={value} value={value}>{value}</option>) }
          </Field>
        </div>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">สาขา</label>
        <div className="col-sm-4">
          <Field component="select" name="major" className="form-control">
              <option value ="000" >------ สาขา -----</option>
              { _.forEach(this.state.majors).map(value => <option key={value[0]} value={value[1]}>{value[1]}</option>)}
          </Field>
        </div>
      </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">รอบที่ผ่านการคัดเลือก</label>
        <div className="col-sm-4">
          <Field component="select" name="round" className="form-control">
              <option value ="0" >------ รอบที่ -----</option>
              <option value ="1">รอบที่ 1/1 (Portfolio)</option>
              <option value ="2">รอบที่ 1/2 (Portfolio)</option>
              <option value ="3">รอบที่ 2 (โควต้า)</option>
              <option value ="4">รอบที่ 3 (รับตรงร่วม)</option>
              <option value ="5">รอบที่ 4 (Admision)</option>
              <option value ="6">รอบที่ 5 (รับตรงอิสระ)</option>
          </Field>
        </div>
      </div>
      <div className="form-group">
          <label className="col-sm-3 control-label">ชื่อโครงการที่เข้าร่วม(ถ้ามี)</label>
          <div className="col-sm-4">
            <Field
                name="projectname"
                component="input"
                type="text"
                className="form-control"
                placeholder="ชื่อโครงการ"
              />
          </div>
        </div>
      <div className="form-group">
        <label className="col-sm-3 control-label">ยืนยันการสมัคร</label>
        <div className="col-sm-4">
          <Field name="confirm" id="isConfirm" component="input" type="checkbox"/>
        </div>
      </div>
     </div>
    }
    else if(this.state.status ==="2"){
      formStatus = <div id="formStatus"> 
      <div className="form-group">
        <label for="uni" className="col-sm-3 control-label">มหาวิทยาลัย</label>
        <div className="col-sm-4">
          <Field
              name="uni"
              component="input"
              type="text"
              className="form-control"
              placeholder="มหาวิทยาลัย"
            />
        </div>
      </div>
      <div className="form-group">
        <label for="faculty" className="col-sm-3 control-label">คณะ</label>
        <div className="col-sm-4">
          <Field
              name="fac"
              component="input"
              type="text"
              className="form-control"
              placeholder="คณะ"
            />
        </div>
      </div>
      <div className="form-group">
        <label for="section" className="col-sm-3 control-label">สาขา</label>
        <div className="col-sm-4">
          <Field
              name="maj"
              component="input"
              type="text"
              className="form-control"
              placeholder="สาขา"
            />
        </div>
      </div>
     </div>
    }
    else if(this.state.status ==="5"){
      formStatus = <div id="formStatus"> 
      <div className="form-group">
      <label className="col-sm-3 control-label">เหตุผล</label>
      <div className="col-sm-4">
        <Field
            name="condition"
            component="input"
            type="text"
            className="form-control"
            placeholder="เหตุผล"
          />
      </div>
    </div>
     </div>
    }
    return(
      <form name="QuestionForm" 
            onSubmit={handleSubmit(e => handleQuestion(e))} 
            className="form-horizontal group-border-dashed">
        <div className="form-group">
          <label className="col-sm-3 control-label">ชื่อ</label>
          <div className="col-sm-4">
            <Field
              name="fname"
              component={renderField}
              className={ styles.textfield }
              type="text"
              placeholder="ชื่อ"
            />
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">นามสกุล</label>
          <div className="col-sm-4">
            <Field
                name="lname"
                component={renderField}
                type="text"
                className="form-control"
                placeholder="นามสกุล"
              />
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">ชื่อเล่น</label>
          <div className="col-sm-4">
            <Field
                name="nickname"
                component={renderField}
                type="text"
                className="form-control"
                placeholder="ชื่อเล่น"
              />
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">เพศ :</label>
          <div className="col-sm-4">
            <label className="control-label"><Field name="gender" component="input" type="radio" value="male" /> ชาย</label>
            <label className="control-label"><Field name="gender" component="input" type="radio" value="female" /> หญิง</label>
          </div>
        </div>
        <div className="form-group">
          <div className="form-group">
            <label className="col-sm-3 control-label">ชั้น</label>
            <div className="col-sm-4">
              <Field component="select" className="form-control" name="room">
                <option key="0" value="0">----- ห้อง -----</option>
                { _.range(1, MAX_ROOM + 1).map(value => <option key={value} value={value}>6/{value}</option>) }
              </Field>
            </div>
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label" >เลขที่</label>
          <div className="col-sm-4">
            <Field component="select" className="form-control" name="no">
              <option key="0" value="0">----- เลขที่ -----</option>
              { _.range(1, MAX_NUM + 1).map(value => <option key={value} value={value}>{value}</option>) }
            </Field>
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-3 control-label">สถานะการศึกษาต่อ</label>
          <div className="col-sm-4">
            <Field component="select" name="status" className="form-control" onChange={this.statusChange} >
                <option value ="0" >------ สถานะ -----</option>
                <option value ="1">ศึกษาต่อในประเทศ</option>
                <option value ="2">ศึกษาต่อต่างประเทศ</option>
                <option value ="3">พักการศึกษา1ปี</option>
                <option value ="4">ไม่จบการหลักสูตร</option>
                <option value ="5">ไม่ศึกษาต่อ(ให้เหตุหล เช่น ประกอบอาชีพ)</option>
            </Field>
          </div>
        </div>
         {formStatus}
        <div className="form-group">
          <p className="text-center">
            <button type="submit" className="btn btn-space btn-primary">Summit</button>
            <button className="btn btn-space btn-default">Cancel</button>
          </p>
        </div>
      </form>
    );
  }
}

export default reduxForm({ form: 'Question',validate })(QuestionForm);