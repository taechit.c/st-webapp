import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import TextField from 'material-ui/TextField'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { Clearfix } from "react-bootstrap";

import fire from '../../configs/fireconfig';
import styles from "./question.scss";

const MAX_NUM = 60;
const MAX_ROOM = 20;
const FIRE_UNI_LISTS = fire.database().ref().child('universitys');

// config validate rule
const validate = (values) => {
  const errors = [];
  const requireFields = [
    'fname',
    'lname',
    'nickname',
    'gender',
    'room',
    'no',
    'status',
    'round',
    'university',
    'faculty',
    'major',
    'uni',
    'fac',
    'maj',
    'condition',
    'confirm',
    'uniText',
    'majText',
    'faculty'
  ];

  requireFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  });

  return errors;
};

const renderTextField = ({
  input,
  label,
  floatingLabelText,
  errorStyle,
  warning,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    floatingLabelText={ floatingLabelText }
    fullWidth={ true }
    errorText={ touched && (error || warning)}
    errorStyle={ errorStyle }
    {...input}
    {...custom}
    />
);

const renderRadioGroup = ({ input, ...rest }) => (
  <RadioButtonGroup
    {...input}
    {...rest}
    valueSelected={input.value}
  />
);

const renderSelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  ...custom
}) => (
  <SelectField
    floatingLabelText={label}
    fullWidth={ true }
    errorText={touched && error}
    {...input}
    onChange={(event, index, value) => input.onChange(value)}
    children={children}
    {...custom}
  />
);

const textfieldStyles = {
  labelStyle:{
    color: '#4EAAF7',
    fontSize: '15px',
    fontWeight: 'bold'
  },
  floatingLabelStyle: {
    color: '#4EAAF7',
    fontSize: '15px',
    fontWeight: 'bold'
  },
  hintStyle: {
    color: '#c4c4c4'
  },
  errorStyle: {
    color: '#ffc672'
  }
};

class QuestionForm extends React.Component {
  render(){
    const { handleSubmit,handleQuestion,pristine, formSelect, submitting } = this.props;
    return(
      <form onSubmit={handleSubmit(e => handleQuestion(e))}>
        <div className={ styles.content_form  }>
          <div className="col-md-12">
            <Field
              id="fname"
              name="fname"
              component={ renderTextField }
              className={ styles.textfield }
              floatingLabelText="ชื่อ"
              floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
              errorStyle={ textfieldStyles.errorStyle }
              type="text"
              autoComplete="off" />
          </div>
          <Clearfix />
          <div className="col-md-12">
            <Field
              id="lname"
              name="lname"
              component={ renderTextField }
              className={ styles.textfield }
              floatingLabelText="นามสกุล"
              floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
              errorStyle={ textfieldStyles.errorStyle }
              type="text"
              autoComplete="off" />
          </div>
          <Clearfix />
          <div className="col-md-12">
            <Field
              id="nickname"
              name="nickname"
              component={ renderTextField }
              className={ styles.textfield }
              floatingLabelText="ชื่อเล่น"
              floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
              errorStyle={ textfieldStyles.errorStyle }
              type="text"
              autoComplete="off" />
          </div>
          <Clearfix />
          <div className="col-md-12">
          <label className={ textfieldStyles.labelStyle }>เพศ</label>
            <Field name="gender" component={renderRadioGroup}>
              <RadioButton value="male" label="ชาย" />
              <RadioButton value="female" label="หญิง" />
            </Field>
          </div>
          <Clearfix />
          <div className="col-md-12">
            <Field
              name="room"
              component={renderSelectField}
              floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
              errorStyle={ textfieldStyles.errorStyle }
              label="ห้อง">
                { _.range(1, MAX_ROOM + 1).map(val => <MenuItem key={val} value={"6/"+val} primaryText={"6/"+val} />) }
            </Field>
          </div>
          <Clearfix />
          <div className="col-md-12">
            <Field
              name="no"
              component={renderSelectField}
              floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
              errorStyle={ textfieldStyles.errorStyle }
              label="เลขที่">
                { _.range(1, MAX_NUM + 1).map(val => <MenuItem key={val} value={""+val} primaryText={val} />) }
            </Field>
          </div>
          <Clearfix />
          <div className="col-md-12">
            <Field
              name="status"
              component={renderSelectField}
              floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
              errorStyle={ textfieldStyles.errorStyle }
              label="สถานะ">
              <MenuItem value="1" primaryText="ศึกษาต่อในประเทศ" />
              <MenuItem value="2" primaryText="ศึกษาต่อต่างประเทศ" />
              <MenuItem value="3" primaryText="พักการศึกษา1ปี" />
              <MenuItem value="4" primaryText="ไม่จบหลักสูตร" />
              <MenuItem value="5" primaryText="ไม่ศึกษาต่อ(ให้เหตุผล เช่น ประกอบอาชีพ)" />
              <MenuItem value="6" primaryText="ลาออก" />
            </Field>
          </div>
          <Clearfix />
          {formSelect}
          <Clearfix />
          <div className="col-md-12">
            <button type="submit" disabled={pristine || submitting}>
              Submit
            </button>
          </div>
        </div>
      </form>
    );
  }
}

QuestionForm = reduxForm({ form: 'QuestionForm', validate })(QuestionForm);

const selector = formValueSelector('QuestionForm')

QuestionForm = connect(state=> {
  
  console.log(state.question);
  const statusValue = selector(state, 'status');
  const universityValue = selector(state, 'university');
  const facultyValue = selector(state, 'faculty');
  const majorValue = selector(state, 'major');
  const uniNamesList = [];
  let facultyList = [];
  let majorList = [];
  let formData;
  
  let formOther;
  let formOtherFac;
  let formOtherMaj;

  facultyList.push('------ คณะ -----');
  
  FIRE_UNI_LISTS.on('value', uni =>{
    uni.forEach(data=>{
      uniNamesList.push(data.key);
    });
  });

  if(universityValue !== undefined){
    const facultyURL = fire.database().ref().child('universitys/'+ universityValue);
    facultyURL.on('value', fac=>{
      fac.forEach(data=>{
        facultyList.push(data.key);
      })
    });
  }

  if(universityValue !== undefined && facultyValue !== undefined){
    const majorURL = fire.database().ref().child('universitys/'+ universityValue +"/" + facultyValue);
    majorURL.on('value', maj => {
      _.forEach(maj.val(),(val,key)=>{
        let inData = [key,val];
        majorList.push(inData);
      });
    });
  }

  if(universityValue ==="999"){
    formOther = (
      <div>
        <Field
          id="uniText"
          name="uniText"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="ชื่อมหาวิทยาลัย"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />

        <Field
          id="facText"
          name="facText"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="ชื่อคณะ"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
          
        <Field
          id="majText"
          name="majText"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="ชื่อสาขา"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
      </div>
    );
  }
  else{
    formOther = (
      <div>
        <Field
          name="faculty"
          component={renderSelectField}
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          label="คณะ">
            { _.each(facultyList).map(val => <MenuItem key={val} value={val} primaryText={val} />) }
            <MenuItem value="999" primaryText="อื่นๆ" />
        </Field>
        <Field
          name="major"
          component={renderSelectField}
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          label="สาขา">
            { _.each(majorList).map(val => <MenuItem key={val[0]} value={val[1]} primaryText={val[1]} />) }
            <MenuItem value="999" primaryText="อื่นๆ" />
        </Field>
      </div>
    );
  }

  if(facultyValue === "999"){
    formOther = (
      <div>
        <Field
          name="faculty"
          component={renderSelectField}
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          label="คณะ">
            { _.each(facultyList).map(val => <MenuItem key={val} value={val} primaryText={val} />) }
            <MenuItem value="999" primaryText="อื่นๆ" />
        </Field>
      </div>
    );
    formOtherFac = (
      <div>
       <Field
          id="facText"
          name="facText"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="ชื่อคณะ"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
          
        <Field
          id="majText"
          name="majText"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="ชื่อสาขา"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
      </div>
    );
  }
  else{
    formOtherFac = <div></div>;
  }
  
  if(majorValue==='999'){
    formOtherMaj = (
      <div>
        <Field
          id="majText"
          name="majText"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="ชื่อสาขา"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
      </div>
    );
  }
  else{
    formOtherMaj = <div></div>;
  }

  if(statusValue === "1"){
    if(uniNamesList.length > 0){
      formData = (<div className="col-md-12">
      <Field
        name="university"
        component={renderSelectField}
        floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
        errorStyle={ textfieldStyles.errorStyle }
        label="มหาวิทยาลัย">
          { _.each(uniNamesList).map(val => <MenuItem key={val} value={val} primaryText={val} />) }
          <MenuItem value="999" primaryText="อื่นๆ" />
      </Field>
      {formOther}
      {formOtherFac}
      {formOtherMaj}
      <Field
        name="round"
        component={renderSelectField}
        floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
        errorStyle={ textfieldStyles.errorStyle }
        label="รอบที่ผ่านการคัดเลือก">
        <MenuItem value="1" primaryText="รอบที่ 1 (Portfolio)" />
        <MenuItem value="2" primaryText="รอบที่ 2 (โควตา)" />
        <MenuItem value="3" primaryText="รอบที่ 3 (รับตรงร่วม)" />
        <MenuItem value="4" primaryText="รอบที่ 4 (Admision)" />
        <MenuItem value="5" primaryText="รอบที่ 5 (รับตรงอิสระ)" />
        <MenuItem value="6" primaryText="รับตรงโดยสถาบัน" />
      </Field>
      <Field
        id="projectname"
        name="projectname"
        component={ renderTextField }
        className={ styles.textfield }
        floatingLabelText="ชื่อโครงการที่เข้าร่วม(เช่น โครงการช้างเผือก,โครงการเด็กดีมีที่เรียน)"
        floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
        errorStyle={ textfieldStyles.errorStyle }
        type="text"
        autoComplete="off" />
      <Field
        name="confirm"
        component={renderSelectField}
        floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
        errorStyle={ textfieldStyles.errorStyle }
        label="การยืนยันสิทธิ์">
        <MenuItem value="yes" primaryText="ยืนยัน" />
        <MenuItem value="no" primaryText="สละสิทธิ์" />
      </Field>
    </div>);
    }

  }
  else if(statusValue === "2"){
    formData = (
      <div className="col-md-6">
        <Field
          id="uni"
          name="uni"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="มหาวิทยาลัย"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
        <Field
          id="fac"
          name="fac"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="คณะ"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
        <Field
          id="maj"
          name="maj"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="สาขา"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
    </div>
    );
  }
  else if(statusValue === "3" || statusValue === "5"){
    formData = (
      <div className="col-md-6">
        <Field
          id="condition"
          name="condition"
          component={ renderTextField }
          className={ styles.textfield }
          floatingLabelText="เหตุผล"
          floatingLabelStyle={ textfieldStyles.floatingLabelStyle }
          errorStyle={ textfieldStyles.errorStyle }
          type="text"
          autoComplete="off" />
      </div>
    );
  }
  const formSelect = formData;
  return{
    statusValue,
    formSelect,
  }
})(QuestionForm);


export default QuestionForm;
