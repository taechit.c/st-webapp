import Questions from './containers/questions';
import Success from './containers/success';
const routes = [
    {
      path: '/',
      component: Questions,
      requireAuth: false
    },
    {
      path : '/success',
      component : Success,
      requireAuth : false
    }
  ];
  
  export default routes;