let initState = {
    question: {
      fname : '----'
    }
  };

const questionReducer = (state = initState, action) => {
    switch (action.type) {
      case 'SUBMIT_SUCCESS':
        //console.log("in action summit");
        //console.log(action);
        return { ...state, ...action.payload, redirect : true };
      case 'SUBMIT_FAIL':
        return { ...state, ...action.payload };
      case 'LOAD' :
        //console.log("in action load");
        //console.log(action);
        return { data : action.data}
      default:
        //console.log("in action default")
        //console.log(state)
        return state; 
    }
  };
  
  export { questionReducer };