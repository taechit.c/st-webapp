import React, {Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

class Success extends React.Component{
    render(){
        return (<Grid>
                    <Row>
                        <Col xs={4} xsOffset={4} ><h2>ขอบคุณที่ร่วมกรอกข้อมูล</h2></Col>
                    </Row>
                </Grid>);
    }
}

export default Success;