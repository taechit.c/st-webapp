import React, {Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { questionSubmit } from '../actions/questionActions';
import QuestionForm from '../components/QuestionForm';


const mapStateToProps = state => {
  return {
    question: state.question
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return bindActionCreators(
    {
      questionSubmit: questionSubmit
    },
    dispatch
  );
};

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };
    this.handleQuestion = this.handleQuestion.bind(this);
  }

  handleQuestion(formValue) {
    this.props.questionSubmit(formValue);
  }
  componentDidMount(){
    document.title = "Dek-Survey : Question";
  }
  componentWillReceiveProps(nextProps) {
    this.setState({redirect: nextProps.question.redirect});
  }
  render() {
    const {redirect} = this.state;
    if (redirect) {
      return <Redirect to='/success'/>;
    }
    return (
      <div className="be-wrapper be-nosidebar-left">
        <nav className="navbar navbar-default navbar-fixed-top be-top-header" >
            <div className="container-fluid">
              <div className="navbar-header"><a href="index.html" className="navbar-brand"></a>
              </div>
              <div className="be-right-navbar">
              <div className="page-title"><span>Dek Survey</span></div>
              </div>
            </div>
          </nav>
          <div className="be-content">
            <div className="page-head">
              <h2 className="page-head-title"></h2>
            </div>
          </div>
          <div className="main-content container-fluid">
            <div className="row">
              <div className="col-md-9">
                <div className="panel panel-default panel-border-color panel-border-color-primary">
                  <div className="panel-heading panel-heading-divider">แบบสำรวจเพื่อติดตามผลการศึกษาต่อของนักเรียนระดับชั้นมัธยมศึกษาปีที่ 6 โรงเรียนเตรียมอุดมศึกษาน้อมเกล้า ประจำปีการศึกษา 2562
                    <span className="panel-subtitle">ครูพฤกษา เลิศวิจิตรจรัส ครูแนะแนว โรงเรียนเตรียมอุดมศึกษาน้อมเกล้า</span>
                  </div>
                  <div className="panel-body">
                    <QuestionForm handleQuestion={this.handleQuestion} />
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Questions);