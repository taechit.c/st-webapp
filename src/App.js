import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './reducers';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import AppContainer from './components/AppContainer';

import './assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css';
import './assets/lib/material-design-icons/css/material-design-iconic-font.min.css';
import './assets/css/style.css';
//import fire from './configs/fireconfig';

const logger = createLogger();
const middleware = applyMiddleware(logger, thunk);
const store = createStore(reducers, middleware);

injectTapEventPlugin();


class App extends Component {
  render() {
    return (
      <Provider store={ store } key="provider">
        <MuiThemeProvider>
          {<AppContainer />}
        </MuiThemeProvider>  
      </Provider>  
    );
  }
}

export default App;
